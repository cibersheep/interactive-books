import QtQuick 2.9
import Ubuntu.Components 1.3

Row {
    width: parent.width
    spacing: units.gu(1)
    property string listText

    Label {
        id: button
        anchors.verticalCenter: parent.verticalCenter
        text: "•"
    }

    Label {
        width: parent.width - button.width - units.gu(1)
        horizontalAlignment: Text.AlignJustify
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        text: listText
    }
}
