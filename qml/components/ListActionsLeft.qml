import QtQuick 2.9
import Ubuntu.Components 1.3
import Backend 1.0
import QtQuick.LocalStorage 2.0

import "../js/db.js" as DB

ListItemActions {
    actions: [
        Action {
            iconName: "delete"
            text: i18n.tr("Delete")
            onTriggered: {
                Backend.removeDir(filePath)
                lastPosition = "null";

                //bookIndex.bookTitle doesn't exist if game is deleted without loading
                //Better use fileName
                DB.storePosition(new Date(), fileName, "null");
            }
        }
    ]
}
