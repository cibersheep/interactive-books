import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

CommonHeader {

    trailingActionBar {
        actions: [
            Action {
                iconName: "info"
                shortcut: "Ctrl+i"
                text: i18n.tr("Information")
                onTriggered: {
                    pageStack.push(Qt.resolvedUrl("../About.qml"));
                }
            },
            Action {
                iconName: "add"
                shortcut: "Ctrl+a"
                text: i18n.tr("Add")
                onTriggered: {
                    Qt.inputMethod.hide();
                    pageStack.push(Qt.resolvedUrl("../ImportPage.qml"),{"contentType": ContentType.All, "handler": ContentHandler.Source})
                }
            }
        ]
    }
}
