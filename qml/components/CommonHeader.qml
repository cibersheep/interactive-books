import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

PageHeader {
    property string icon

    StyleHints {
        foregroundColor: txtColor
        backgroundColor: lightColor
        dividerColor.visible: false
    }
}
