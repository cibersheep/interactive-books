import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0

import Qt.labs.platform 1.0
import Backend 1.0
import "components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'interactivebooks.cibersheep'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    readonly property color darkColor: "#101010"
    readonly property color lightColor: "#115f6b"
    readonly property color txtColor: "#e7e7e7"
    readonly property color highLightColor: "#cfdfe1"
    property bool isLandscape: width > height

    //M10 HD is 160 gu on landscape (default scaling)
    property bool isBigScreen: width > units.gu(150)
    property int gridmargin: units.gu(2)
    property string lastPosition: "null"
    property string gamesFolder: StandardPaths.writableLocation(StandardPaths.CacheLocation) + "/Games/"

    //Hack. The StandardPaths starts as "file://" on desktop and ?
    onGamesFolderChanged: gamesFolder = gamesFolder.replace("file://","")

    Settings {
        id: generalSettings
        category: "General_Settings"
        property bool firstRun: true
    }

    PageStack {
        id: pageStack

        Component.onCompleted: {
            pageStack.push(mainPage);
        }
    }

    MainPage {
        id: mainPage
        anchors.fill: parent
    }
}
