import QtQuick 2.9
import Ubuntu.Components 1.3
import Qt.labs.folderlistmodel 2.1

import Backend 1.0
import "components"

Page {
    property var indexJson

    header: MainPageHeader {
        id: mainHeader
        title: i18n.tr('Interactive Books')
        flickable: gameStoriesView
    }

    Component.onCompleted: {
        // We can check the folder on any run
        console.log("PATH",gamesFolder)
        Backend.createGamesPath(gamesFolder);

        storiesFolderModel.folder     = gamesFolder
        storiesFolderModel.rootFolder = gamesFolder
    }

    ListView {
        id: gameStoriesView
        // TODO: Finish on next release
        //width: isLandscape ? parent.width / 2 : parent.width
        width: parent.width

        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            topMargin: gridmargin / 2
        }

        model: storiesModel
        delegate: storiesDelegate
        header: storiesMainIcon

        Component {
            id: storiesMainIcon

            PaddedIcon {
                width: parent.width
                height: units.gu(10)
                visible: storiesModel.count !== 0
            }
        }

        Component {
            id: storiesDelegate

            ListItem {
                //height: storiesDelegateLayout.height
                divider.visible: false
                clip: true
                highlightColor: highLightColor
                leadingActions:  ListActionsLeft {}
                //TODO: In the next release
                //trailingActions: ListActionsRight {}

                ListItemLayout {
                    id: storiesDelegateLayout
                    title.text: fileName
                    //subtitle.text: secondaryText
                    ProgressionSlot {}
               }

               onClicked: importJson(filePath)
            }
        }
    }

    FolderListModel {
        id: storiesFolderModel
        folder: gamesFolder
        rootFolder: gamesFolder

        //We do this onCompleted. Otherwise on first run is listing the default /op folder
        //folder: "/home/phablet/.cache/interactivebooks.cibersheep/Games/"
        showDotAndDotDot: false
        showHidden: false
        showDirs: true
        sortField: FolderListModel.Name

        onCountChanged: storiesModel.updateModels()
    }

    ListModel {
        id: storiesModel

        function updateModels() {
            console.log("Updating Models")
            storiesModel.clear()
            for (var i=0; i< storiesFolderModel.count; ++i) {
                if (storiesFolderModel.get(i, "fileIsDir")) {
                    //DEBUG: console.log("A directory: " + storiesFolderModel.get(i, "filePath"))
                    storiesModel.append({ fileName: storiesFolderModel.get(i, "fileName"), filePath: storiesFolderModel.get(i, "filePath") })
                }
            }
        }
    }

    Loader {
        id: emptyStateLoader
        anchors.fill: parent
        active: storiesModel.count === 0
        source: Qt.resolvedUrl("components/EmptyDocument.qml")
    }

    Connections {
        target: Backend

        onGamesFolderChanged: {
            storiesFolderModel.rootFolder = ""
            storiesFolderModel.folder = ""
            storiesFolderModel.rootFolder = gamesFolder
            storiesFolderModel.folder = gamesFolder
        }
    }

/*
 * TODO: finish on next release
    Component {
        id: secondFlickComp

        Flickable {
            id: secondaryFlick

        }
    }
    Loader {
        id: flickableOnLandscape
        active: isLandscape
        sourceComponent: secondFlickComp
        width: parent.width / 2

        anchors {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
            topMargin: mainHeader.height
        }
    }
*/

    EmptyBottomEdge {
    }

    //Change this to cpp
    //https://stackoverflow.com/questions/15893040/how-to-create-read-write-json-files-in-qt5
    function importJson(filePath) {
        var bookPath = "file://" + filePath
        var xhr = new XMLHttpRequest;
        xhr.open("GET", bookPath + "/index.json");

        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var response = xhr.responseText;
                try {
                    indexJson = JSON.parse(response);
                    pageStack.push(Qt.resolvedUrl("BookViewer.qml"),{"bookPath": bookPath, "bookIndex": indexJson})
                } catch(e) {
                    if (e instanceof SyntaxError) {
                        console.log("Syntx Error: Show a message to the user")
                    }
                }
            }
        };

        xhr.onerror = function () {
            error(xhr, xhr.status);
        };

        xhr.send();
    }
}
