/*
 * About template
 * By Joan CiberSheep using base file from uNav
 *
 * uNav is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * uNav is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.DownloadManager 1.2

import "components"
import Backend 1.0

Page {
    id: downloadPage

    signal downloadFinished

    header: CommonHeader {
        id: pageHeader
        title: i18n.tr("Latest Story Links")
        flickable: downloadFlickable
    }

    Flickable {
        id: downloadFlickable

        anchors {
            fill: parent
            bottomMargin: gridmargin
        }

        ListModel {
            id: gameStoriesModel

            Component.onCompleted: initialize()

            function initialize() {
                gameStoriesModel.append({ category: i18n.tr("Downloads available"), mainText: "Zork The Forces Of Krill (1.0)", secondaryText: i18n.tr("from %1").arg("archive.org"), link: "https://archive.org/download/ZorkTheForcesOfKrill/Zork-The_Forces_of_Krill-%281.0%29.ibs" })
            }
        }

        ListView {
            id: gameStoriesView
            anchors.fill: parent
            model: gameStoriesModel
            delegate: listDelegate
            section.property: "category"
            section.criteria: ViewSection.FullString
            section.delegate: ListItemHeader {
                title: section
            }
        }

        Component {
            id: listDelegate

            ListItem {
                divider.visible: false
                highlightColor: highLightColor

                ListItemLayout {
                    id: storiesDelegateLayout
                    title.text: mainText
                    subtitle.text: secondaryText
                    ProgressionSlot { name: progressBar.visible ? "" : "save"}
                }

                onClicked: {
                    if (progressBar.value == 0) {
                        single.download(link);
                        //progressBar.visible = true;
                    }
                }

                ProgressBar {
                    id: progressBar
                    minimumValue: 0
                    maximumValue: 100
                    //This will only work on OTA7+
                    visible: single.downloadInProgress
                    value: single.progress
                    width: parent.width - 2 * gridmargin

                    anchors {
                        horizontalCenter: parent.horizontalCenter
                        bottom: parent.bottom
                    }

                    SingleDownload {
                        id: single
                        autoStart: true

                        metadata: Metadata {
                            id: downloadMetadata
                            showInIndicator: true
                            title: link.split("/")[link.split("/").length-1]
                        }

                        onDownloadingChanged: console.log("DEBUG: downloadInProgress is " + downloadInProgress)

                        onErrorFound: {
                            console.log("[ERROR]: " + download.errorMessage)
                        }

                        onFinished: {
                            //progressBar.visible = false;
                            progressBar.value = 0;
                            console.log("Download path: " + path + " title " + downloadMetadata.title.replace(/%28/g,"(").replace(/%29/g,")"));
                            Backend.extractZip(path, gamesFolder);
                            downloadFinished();
                        }
                    }
                }
            }
        }
    }


}
