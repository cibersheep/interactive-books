import QtQuick 2.9
import Ubuntu.Components 1.3

import "components"
import "js/utils.js" as JS
import "js/jsonrender.js" as JSONrender

Flickable {
    id: mainFlick
    property string firstText
    property string textJSON: bookPath + "/" + firstText
    property bool isSecondColEmpty: secondCol.children.length < 1
    
    anchors.fill: parent
    contentHeight: isLandscape ? Math.max(col.height, secondCol.height) : col.height + secondCol.height + gridmargin

    Component.onCompleted: {        
        importJson(textJSON);

        //Why set flickable here? If user resets game, Flickable goes under the header ¿bug?
        bookHeader.flickable = mainFlick;
    }

    //Main element to dynamically create the content in, using javascript (JSONrender)
    Column {
        id: col
        width: isSecondColEmpty || !isLandscape ? Math.min(units.gu(90), parent.width) : parent.width / 2 - gridmargin
        spacing: gridmargin

        anchors {
            top: parent.top
            topMargin: gridmargin
            left: isSecondColEmpty || !isLandscape ? undefined : parent.left
            horizontalCenter: isSecondColEmpty || !isLandscape ? parent.horizontalCenter : undefined
        }
    }

    Column {
        id: secondCol
        width: isSecondColEmpty || !isLandscape ? Math.min(units.gu(90), parent.width) : parent.width / 2 - gridmargin
        spacing: gridmargin

        anchors {
            top: isLandscape ? parent.top : col.bottom
            topMargin: gridmargin
            right: parent.right
            horizontalCenter: isSecondColEmpty || !isLandscape ? parent.horizontalCenter : undefined
        }

        Component {
            id: secondColEmpty

            Image {
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
                fillMode: Image.PreserveAspectFit
                source: "../assets/floral.svg"
                sourceSize.width: mainPage.width > units.gu(60) ? units.gu(90) : units.gu(40)
                
                Component.onCompleted: width = parent.width
            }
        }
    }

/*
    Loader {
        id: loadCoverIfSecondColIsEmpty
        active: isLandscape && isSecondColEmpty
        sourceComponent: secondColEmpty
        width: parent.width / 2 - gridmargin
        height: parent.height

        anchors {
            right: parent.right
            topMargin: bookHeader.height
        }
    }
    */

    //Change this to cpp
    function importJson(jsonURL) {
        console.log("Importing json text " + jsonURL)
        var xhr = new XMLHttpRequest;
        xhr.open("GET", jsonURL);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var response = xhr.responseText;
                try {
                    //DEBUG: console.log("Try parsing the json")
                    var textJson = JSON.parse(response);
                    JSONrender.updateJSON(textJson)
                } catch(e) {
                    if (e instanceof SyntaxError) {
                        console.log("Couldn't import json: Syntx Error")
                    } else console.log("Couldn't import json: " + e) 
                }
            }
            
            
        };
        xhr.onerror = function () { 
            error(xhr, xhr.status); 
        };
        xhr.send();
    }
}
